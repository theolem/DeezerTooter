function tootInfo(message) {
  tootContent = document.getElementById("toot-info").innerHTML
  tootContent += "\n\n"
  tootContent += document.getElementById("toot-comment").value
  tootContent += "\n\n"
  tootContent += document.getElementById('toot-url').innerText
  tootContent += "\n\n"
  tootContent += document.getElementById("toot-hashtags").value
  url = "http://localhost:5000/toot"
  fetch(url, {
    method: 'POST',
    body: tootContent
  })
    .then(function (response) {
      return response.json();
    })
    .catch(function (error) {
      console.log("Error while tooting: " + error);
    })
}

function handleResponse(message) {
  document.getElementById("toot-info").innerHTML = message.artist + " - " + message.title
  document.getElementById("toot-hashtags").value = "#radio #np #musique #music" + '\n'
  document.getElementById('toot-url').innerText = message.url
}

function handleError(error) {
  console.log(`Error: ${error}`);
}

function getCurrentInfo () {
  var gettingActiveTab = browser.tabs.query({active: true, currentWindow: true})
  gettingActiveTab.then((tabs) => {
    browser.tabs.sendMessage(tabs[0].id, {command: "get-info"})
    .then(handleResponse, handleError);
  })
}

button = document.getElementById('toot-button')
button.onclick = tootInfo
document.addEventListener("click", getCurrentInfo)
browser.tabs.executeScript(null, { file: "/content_script.js" });