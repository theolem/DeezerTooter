browser.runtime.onMessage.addListener((data, sender, sendResponse) => {
  deezerTitle = document.getElementsByClassName('track-link')[0].innerHTML
  deezerArtist = document.getElementsByClassName('track-link')[1].innerHTML
  sendResponse({title: deezerTitle, artist: deezerArtist, url: window.location.href})
});