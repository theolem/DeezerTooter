# DeezerTooter

Mastodon bot that toos the currently playing music on Deezer to Mastodon.

The bot is composed of :
* a [Flask](https://flask.palletsprojects.com/en/1.1.x/)-based bot that makes use of the [Mastodon.py library](https://github.com/halcy/Mastodon.py) to access the Mastodon API.
* a Firefox add-on, enabled on **Deezer.com** only, that allows the one-click sharing of the current playing song, plus some configurable comment and hashtag.

## ⚙ Set up

### Run the bot
* clone the repo
* install the dependencies : `pip3 install requirements.txt`. Consider using a [virtual environment](https://python-guide-pt-br.readthedocs.io/fr/latest/dev/virtualenvs.html) for your dependencies.
* copy the config file : `cp config_default.py config.py`
* edit `config.py` with your own server name and API token.
* export environment variables and run the Flask app : `export FLASK_APP = app.py` then `flask run`

### Set up the add-on
This add-on is not distributed, so for now you'll have to browse to `about:debugging`, "This firefox", then "Add temporary add-on".

And you shoud be good to go !