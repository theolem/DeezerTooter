from mastodon import Mastodon
from flask import Flask, request
from config import configObject

# The Flask instance
app = Flask(__name__)

mastodon = Mastodon(
    access_token = configObject.ACCESS_TOKEN,
    api_base_url = configObject.SERVER_NAME
)

@app.route('/toot', methods = ['POST'])
def toot():
  if(request.method != 'POST'):
    print('/toot/ : You should only POST on this path.')
    return
  toot_content = request.data
  if(toot_content == None):
    return "No content, aborting"
  toot_content = toot_content.decode("utf-8")
  print(toot_content)
  print(mastodon.status_post(toot_content, spoiler_text="Musique"))
  return "Tooted !"